require 'spec_helper'
require 'rails_helper'

describe SessionsController do
  describe "GET new" do
    it "renders the new template for unauthed users" do
      get :new
      expect(response).to render_template :new
    end

    it "redirects to home for authed uesrs" do
      session[:user_id] = Fabricate(:user).id
      get :new
      expect(response).to redirect_to home_path
    end
  end

  describe "POST create" do
    context "With valid creds" do
      let(:alice) { Fabricate(:user) }
      before do
        post :create, params: { email: alice.email, password: alice.password }
      end

      it "puts the logged in user in the session" do
        expect(session[:user_id]).to eq(alice.id)
      end

      it "redirects to the home" do
        expect(response).to redirect_to home_path
      end

      it "sets the success flash" do
        expect(flash[:success]).not_to be_blank
      end
    end

    context "With invalid creds" do
      before do
        bob = Fabricate(:user)
        post :create, params: { email: bob.email, password: 'incorrect' }
      end

      it "does not put the user in the session" do
        expect(session[:user_id]).to be_nil
      end

      it "redirects to the login" do
        expect(response).to redirect_to login_path
      end

      it "sets the error flash" do
        expect(flash[:danger]).not_to be_blank
      end
    end
  end

  describe "GET destroy" do
    before do
      session[:user_id] = Fabricate(:user).id
      get :destroy
    end

    it "clear session for the user" do
      expect(session[:user_id]).to be_nil
    end

    it "redirects to login" do
      expect(response).to redirect_to login_path
    end

    it "sets the success flash" do
      expect(flash[:success]).not_to be_blank
    end
  end
end