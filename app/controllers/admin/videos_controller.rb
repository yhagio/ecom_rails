class Admin::VideosController < AdminController
  def new
    @video = Video.new
  end

  def create
    @video = Video.new video_params
    if @video.save
      flash[:success] = "You created new video"
      redirect_to new_admin_video_path
    else
      flash[:danger] = "There are invalid inputs"
      render :new
    end
  end

  private

  def video_params
    params.require(:video).permit(
      :title,
      :description,
      :small_cover,
      :large_cover,
      :video_url,
      category_ids: []
    )
  end
end