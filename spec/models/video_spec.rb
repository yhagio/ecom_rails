require 'spec_helper'
require 'rails_helper'

describe Video do
  it { should have_many(:categories) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
  # it { should validate_presence_of(:video_url) }

  it "saves itself" do
    video = Video.new(title: 'test123', description: 'test desc123', video_url: 'some_url')
    video.save
    expect(Video.first.title).to eql('test123')
  end

  it "does not save if no title is given" do
    Video.create(title: '', description: 'test desc123', video_url: 'some_url')
    expect(Video.count).to eq(0)
  end

  it "does not save if no description is given" do
    Video.create(title: 'test123', description: '', video_url: 'some_url')
    expect(Video.count).to eq(0)
  end

  # it "does not save if no video url is given" do
  #   Video.create(title: 'test123', description: 'test desc123', video_url: '')
  #   expect(Video.count).to eq(0)
  # end

  it "has a category" do
    drama = Category.create(name: "drama")
    video = Video.create(title: 'test123', description: 'test desc123', video_url: 'some_url')
    VideoCategory.create(video_id: video.id, category_id: drama.id)
    expect(video.categories.first).to eql(drama)
  end

  describe "search_by_title" do
    let!(:video1) do # '!' evaluate immediately (avoid lazy evaluation)
      Video.create(title: 'test123', description: 'test desc123', video_url: 'some_url')
    end

    it "returns an empty array if there is no match" do
      expect(Video.search_by_title("hello")).to eq([])
    end

    it "returns an array of videos if there are exact matches" do
      expect(Video.search_by_title("test123")).to eq([video1])
    end

    it "returns an array of videos if there are partial matches" do
      expect(Video.search_by_title("est")).to eq([video1])
    end

    it "returns an array of videos if there are partial matches with case-incensitive" do
      expect(Video.search_by_title("EST")).to eq([video1])
    end

    it "returns an empty array if there is blank term given" do
      expect(Video.search_by_title("")).to eq([])
    end

    it "returns an array of videos ordered by created_at DESC if there are matches" do
      video2 = Video.create(title: 'test1234', description: 'test desc1234', video_url: 'some_url_2')
      video3 = Video.create(title: 'test1235', description: 'test desc1235', video_url: 'some_url_3')

      expect(Video.search_by_title("test")).to eq([video3, video2, video1])
    end
  end

  describe ".search", :elasticsearch do
    let(:refresh_index) do
      Video.import
      Video.__elasticsearch__.refresh_index!
    end
    let(:user) { Fabricate(:user) }

    context "with title" do
      it "returns no results when there's no match" do
        Fabricate(:video, title: "Futurama")
        refresh_index

        expect(Video.search("whatever").records.to_a).to eq []
      end

      it "returns an empty array when there's no search term" do
        futurama = Fabricate(:video)
        south_park = Fabricate(:video)
        refresh_index

        expect(Video.search("").records.to_a).to eq []
      end

      it "returns an array of 1 video for title case insensitve match" do
        futurama = Fabricate(:video, title: "Futurama")
        south_park = Fabricate(:video, title: "South Park")
        refresh_index

        expect(Video.search("futurama").records.to_a).to eq [futurama]
      end

      it "returns an array of many videos for title match" do
        star_trek = Fabricate(:video, title: "Star Trek")
        star_wars = Fabricate(:video, title: "Star Wars")
        refresh_index

        expect(Video.search("star").records.to_a).to match_array [star_trek, star_wars]
      end
    end

    context "with title and description" do
      it "returns an array of many videos based for title and description match" do
        star_wars = Fabricate(:video, title: "Star Wars")
        about_sun = Fabricate(:video, description: "sun is a star")
        refresh_index

        expect(Video.search("star").records.to_a).to match_array [star_wars, about_sun]
      end
    end

    context "multiple words must match" do
      it "returns an array of videos where 2 words match title" do
        star_wars_1 = Fabricate(:video, title: "Star Wars: Episode 1")
        star_wars_2 = Fabricate(:video, title: "Star Wars: Episode 2")
        bride_wars = Fabricate(:video, title: "Bride Wars")
        star_trek = Fabricate(:video, title: "Star Trek")
        refresh_index

        expect(Video.search("Star Wars").records.to_a).to match_array [star_wars_1, star_wars_2]
      end
    end

    context "with title, description and reviews" do
      it 'returns an an empty array for no match with reviews option' do
        star_wars = Fabricate(:video, title: "Star Wars")
        batman    = Fabricate(:video, title: "Batman")
        batman_review = Fabricate(:review, video: batman, content: "such a star movie!", user: user)
        refresh_index

        expect(Video.search("no_match", reviews: true).records.to_a).to eq([])
      end

      it 'returns an array of many videos with relevance title > description > review' do
        about_sun = Fabricate(:video, description: "the sun is a star!")
        star_wars = Fabricate(:video, title: "Star Wars")
        batman    = Fabricate(:video, title: "Batman")
        batman_review = Fabricate(:review, video: batman, content: "such a star movie!", user: user)
        refresh_index

        expect(Video.search("star", reviews: true).records.to_a).to eq([star_wars, about_sun, batman])
      end
    end

    context "filter with average ratings" do
      let(:star_wars_1) { Fabricate(:video, title: "Star Wars 1") }
      let(:star_wars_2) { Fabricate(:video, title: "Star Wars 2") }
      let(:star_wars_3) { Fabricate(:video, title: "Star Wars 3") }

      before do
        Fabricate(:review, rating: "2", video: star_wars_1, user: user)
        Fabricate(:review, rating: "4", video: star_wars_1, user: user)
        Fabricate(:review, rating: "4", video: star_wars_2, user: user)
        Fabricate(:review, rating: "2", video: star_wars_3, user: user)
        refresh_index
      end

      context "with only rating_from" do
        it "returns an empty array when there are no matches" do
          expect(Video.search("Star Wars", rating_from: "4.1").records.to_a).to eq []
        end

        it "returns an array of one video when there is one match" do
          expect(Video.search("Star Wars", rating_from: "4.0").records.to_a).to eq [star_wars_2]
        end

        it "returns an array of many videos when there are multiple matches" do
          expect(Video.search("Star Wars", rating_from: "3.0").records.to_a).to match_array [star_wars_2, star_wars_1]
        end
      end

      context "with only rating_to" do
        it "returns an empty array when there are no matches" do
          expect(Video.search("Star Wars", rating_to: "1.5").records.to_a).to eq []
        end

        it "returns an array of one video when there is one match" do
          expect(Video.search("Star Wars", rating_to: "2.5").records.to_a).to eq [star_wars_3]
        end

        it "returns an array of many videos when there are multiple matches" do
          expect(Video.search("Star Wars", rating_to: "3.4").records.to_a).to match_array [star_wars_1, star_wars_3]
        end
      end

      context "with both rating_from and rating_to" do
        it "returns an empty array when there are no matches" do
          expect(Video.search("Star Wars", rating_from: "3.4", rating_to: "3.9").records.to_a).to eq []
        end

        it "returns an array of one video when there is one match" do
          expect(Video.search("Star Wars", rating_from: "1.8", rating_to: "2.2").records.to_a).to eq [star_wars_3]
        end

        it "returns an array of many videos when there are multiple matches" do
          expect(Video.search("Star Wars", rating_from: "2.9", rating_to: "4.1").records.to_a).to match_array [star_wars_1, star_wars_2]
        end
      end
    end
  end
end
