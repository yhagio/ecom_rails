require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  mount StripeEvent::Engine, at: '/stripe_events'

  root to: 'pages#front'

  get 'ui/:name', to: 'ui#index'

  namespace :admin do
    resources :videos, only: [:new, :create]
    resources :payments, only: [:index]
  end

  get "home", to: "videos#index"

  get "register", to: "users#new"
  get "register/:token", to: "users#new_with_invitation_token", as: "register_with_token"
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy'
  get 'forgot_password', to: 'forgot_passwords#new'
  resources :forgot_passwords, only: [:create]
  get 'forgot_password_confirmation', to: 'forgot_passwords#confirm'
  resources :password_resets, only: [:show, :create]
  get 'expired_token', to: 'pages#expired_token'

  resources :users, only: [:create, :show, :update]
  get 'account', to: 'users#edit'
  get 'billing', to: 'users#plan_and_billing'
  post 'billing', to: 'users#plan_and_billing'

  resources :videos, only: [:index, :show] do
    collection do
      post :search, to: "videos#search"
      get :advanced_search, to: "videos#advanced_search"
    end

    resources :reviews, only: [:create]
  end

  resources :categories, only: [:index, :show]

  get 'my_queue', to: 'queue_items#index'
  resources :queue_items, only: [:create, :destroy]
  post 'update_queue', to: 'queue_items#update_queue'

  get 'people', to: 'relationships#index'
  resources :relationships, only: [:create, :destroy]

  resources :invitations, only: [:new, :create]
end
