class ApplicationMailer < ActionMailer::Base
  default from: 'info@ecomrails.com'
  layout 'mailer'

  def send_welcome_email(user)
    @user = user
    mail to: user.email, from: 'info@ecomrails.com', subject: "Welcome to EcomRails"
  end

  def send_forgot_password(user)
    @user = user
    mail to: user.email, from: 'info@ecomrails.com', subject: "Reset your password"
  end

  def send_invitation_email(invitation)
    @invitation = invitation
    mail to: invitation.recipient_email, from: 'info@ecomrails.com', subject: "Invitation to EcomRails"
  end
end
