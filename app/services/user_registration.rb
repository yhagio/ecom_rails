class UserRegistration
  attr_reader :error_message

  def initialize(user)
    @user = user
  end

  def register(stripe_token, invitation_token)
    if @user.valid?
      customer = creates_customer(@user.email, stripe_token)

      if customer.successful?
        charge = charge_subscription(customer.customer_token)

        if charge.successful?
          @user.customer_token = customer.customer_token
          @user.save
          handle_invitation(invitation_token)
          send_welcome_email(@user)
          @status = :success
          self
        else
          undo_customer_creation(customer.customer_token)
          @status = :failed
          @error_message = charge.error_message
          self
        end

      else
        @status = :failed
        @error_message = customer.error_message
        self
      end

    else
      @status = :failed
      @error_message = "Check your inputs, and try again"
      self
    end
  end

  def successful?
    @status == :success
  end


  private


  def handle_invitation(invitation_token)
    if invitation_token.present?
      invitation = Invitation.find_by(token: invitation_token)
      @user.follow(invitation.inviter)
      invitation.inviter.follow(@user)
      invitation.update_column(:token, nil)
    end
  end

  def creates_customer(email, token)
    StripeWrapper::Customer.create({
      email: email,
      source: token
    })
  end

  def undo_customer_creation(customer)
    StripeWrapper::Customer.delete(customer)
  end

  def charge_subscription(customer)
    StripeWrapper::Subscription.create({
      customer: customer
    })
  end

  def send_welcome_email(user)
    ApplicationMailer.send_welcome_email(user).deliver_later(wait: 10.seconds)
  end
end
