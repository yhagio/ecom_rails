class Video < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  index_name ["ecomrails", Rails.env].join("_")

  # default_scope { order('created_at DESC') }

  # has_many :videos
  # belongs_to :category
  has_many :video_categories
  has_many :categories, through: :video_categories

  has_many :reviews

  validates :title, presence: true, uniqueness: true
  validates :description, presence: true, uniqueness: true
  # validates :video_url, presence: true, uniqueness: true

  mount_uploader :large_cover, LargeCoverUploader
  mount_uploader :small_cover, SmallCoverUploader

  def self.search_by_title(term)
    return [] if term.blank?
    where("LOWER(title) LIKE ?", "%#{term.downcase}%").order("created_at DESC")
  end

  def rating
    reviews.average(:rating).round(1) if reviews.average(:rating)
  end

  # override what data will be send to elasticsearch for indexing
  def as_indexed_json(options={})
    as_json(
      methods: [:rating],
      only: [:title, :description],
      include: {
        reviews: { only: [:content] }
      }
    )
  end

  # search via elasticsearch
  def self.search(search_term, options={})
    query = {
      query: {
        bool: {
          must: {
            multi_match: {
              query: search_term,
              fields: ["title^100", "description^50"],
              operator: "and"
            }
          }
        }
      }
    }

    if search_term.present? && options[:reviews].present?
      query[:query][:bool][:must][:multi_match][:fields] << "reviews.content"
    end

    if options[:rating_from].present? || options[:rating_to].present?
      query[:query][:bool][:filter] = {
        range: {
          rating: {
            gte: (options[:rating_from] if options[:rating_from].present?),
            lte: (options[:rating_to] if options[:rating_to].present?)
          }
        }
      }
    end

    __elasticsearch__.search(query)
  end
end