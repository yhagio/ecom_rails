class AdminController < ApplicationController
  before_action :require_user
  before_action :require_admin

  private

  def require_admin
    access_denied unless logged_in? and current_user.admin?
  end
end