class QueueItem < ActiveRecord::Base
  default_scope { order('position') }

  belongs_to :user
  belongs_to :video

  validates_numericality_of :position, { only_integer: true }

  delegate :title, to: :video, prefix: :video

  def rating
    review.rating if review
  end

  def rating=(new_rating)
    if review
      review.update_column(:rating, new_rating)
    else
      review = Review.new(user: user, video: video, rating: new_rating)
      review.save(validate: false)
    end
  end

  def category
    video.categories[0] if video and video.categories
  end

  def category_name
    video.categories[0].name if video and video.categories
  end

  private

  def review
    @review ||= Review.where(user_id: user.id, video_id: video.id).first
  end
end