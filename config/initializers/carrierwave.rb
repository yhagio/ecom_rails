CarrierWave.configure do |config|

  if Rails.env.production?
    config.fog_provider = 'fog/aws'                            # required
    config.fog_credentials = {
      provider:              'AWS',                            # required
      aws_access_key_id:     ENV['AWS_ACCESS_KEY_ID'],         # required unless using use_iam_profile
      aws_secret_access_key: ENV['AWS_ACCESS_SECRET'],         # required unless using use_iam_profile
      use_iam_profile:       ENV['AWS_USE_IAM'] || false,      # optional, defaults to false
      region:                ENV['AWS_REGION'] || 'us-east-1', # optional, defaults to 'us-east-1'
      host:                  ENV['AWS_HOST'] || nil,           # optional, defaults to nil
      endpoint:              ENV['AWS_ENDPOINT'] || nil        # optional, defaults to nil
    }
    config.fog_directory  = ENV['AWS_DIRECTORY']               # required
    config.fog_public     = ENV['AWS_PUBLIC']                  # optional, defaults to true
    config.fog_attributes = { }                                # optional, defaults to {}
  else
    config.storage = :file
    config.enable_processing = true
  end
end