require 'spec_helper'
require 'rails_helper'

feature "user interacts with the queue" do
  scenario "user adds and reorders videos in the queue" do
    monk = Fabricate(:video, title: "Monk") do
      categories(count: 1) { Fabricate(:category) }
    end
    south_park = Fabricate(:video, title: "South Park") do
      categories(count: 1) { Fabricate(:category) }
    end
    futurama = Fabricate(:video, title: "Futurama") do
      categories(count: 1) { Fabricate(:category) }
    end

    user_login

    add_video_to_queue(monk)
    expect(page).to have_content(monk.title)

    visit video_path(monk)
    expect(page).not_to have_content "+ My Queue"

    add_video_to_queue(south_park)
    add_video_to_queue(futurama)

    set_video_position(monk, 3)
    set_video_position(south_park, 2)
    set_video_position(futurama, 1)

    click_button "Update Instant Queue"

    expect_video_position(monk, 3)
    expect_video_position(south_park, 2)
    expect_video_position(futurama, 1)
  end

  def set_video_position(video, position)
    find("input[data-video-id='#{video.id}']").set(position)
  end

  def expect_video_position(video, position)
    expect(find("input[data-video-id='#{video.id}']").value).to eq(position.to_s)
  end

  def add_video_to_queue(video)
    visit home_path
    click_on_video_on_home_page(video)
    click_link "+ My Queue"
  end
end