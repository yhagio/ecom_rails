document.addEventListener("DOMContentLoaded", function (event) {
  function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    form.submit();
  }

  function createToken() {
    stripe.createToken(card).then(function (result) {
      if (result.error) {
        // Inform the user if there was an error
        var errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
      } else {
        // Send the token to your server
        stripeTokenHandler(result.token);
      }
    });
  };

  if (window.location.pathname.includes("register")) {
    // var stripe = Stripe("#{ENV['STRIPE_PUBLISHABLE_KEY']}");
    var stripe = Stripe(window.stripe_key);

    var elements = stripe.elements();
    var card = elements.create('card', {
      style: {},
      classes: {},
    });

    card.mount('#card-element');

    // Create a token when the form is submitted.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (e) {
      e.preventDefault();
      createToken();
    });
  }
});
