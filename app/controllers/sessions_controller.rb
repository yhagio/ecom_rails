class SessionsController < ApplicationController
  def new
    redirect_to home_path if current_user
  end

  def create
    user = User.find_by(email: params[:email])

    if user && user.authenticate(params[:password])
      if user.active?
        login_user!(user)
      else
        flash[:danger] = "Your account has been deactivated. Please contact support."
        redirect_to login_path
      end
    else
      flash[:danger] = "Email or Password is incorrect"
      redirect_to login_path
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = "You are logged out"
    redirect_to login_path
  end

  def login_user!(user)
    session[:user_id] = user.id
    flash[:success] = "You are logged in"
    redirect_to home_path
  end
end