class UsersController < ApplicationController
  before_action :require_user, only: [:show]

  def new
    redirect_to home_path if current_user
    @user = User.new
  end

  def new_with_invitation_token
    invitation = Invitation.find_by(token: params[:token])
    if invitation
      @user = User.new(email: invitation.recipient_email)
      @invitation_token = invitation.token
      render :new
    else
      redirect_to expired_token_path
    end
  end

  def create
    @user = User.new(user_params)

    result = UserRegistration.new(@user).register(params[:stripeToken], params[:invitation_token])

    if result.successful?
      flash[:success] = "Welcome! Please login."
      redirect_to login_path
    else
      flash[:danger] = result.error_message
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update_attributes(user_params)
      flash[:success] = "Updated Account Information"
      redirect_to home_path
    else
      render :edit
    end
  end

  def plan_and_billing
    customer = StripeWrapper::Customer.retrieve(current_user)
    @customer = StripeCustomerDecorator.new(customer)
    @invoices = StripeWrapper::Invoice.list(current_user).map { |invoice| StripeInvoiceDecorator.new(invoice) }
    cancel_subscription if params[:cancel].present?
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :full_name)
  end

  def cancel_subscription
    cancellation = StripeWrapper::Subscription.cancel_subscription(current_user)
    if cancellation.successful?
      current_user.deactivate!
      session[:user_id] = nil
      flash[:success] = "Your account is no longer active"
      redirect_to login_path
    else
      flash[:danger] = result.error_message
      render :plan_and_billing
    end
  end
end