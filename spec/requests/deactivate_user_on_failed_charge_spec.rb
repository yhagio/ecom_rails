require 'spec_helper'
require 'rails_helper'

describe "Stripe Webhook Events" do
  def bypass_event_signature(payload)
    event = Stripe::Event.construct_from(JSON.parse(payload, symbolize_names: true))
    expect(Stripe::Webhook).to receive(:construct_event).and_return(event)
  end

  describe "charge.failed event" do
    let!(:alice) { Fabricate(:user, customer_token: "cus_00000000000000") }
    let(:payload) { File.read("spec/fixtures/stripe_charge_failed.json") }
    before(:each) { bypass_event_signature payload }

    it "is successful" do
      post '/stripe_events', params: payload
      expect(response.code).to eq "200"
    end

    it "deactivates user" do
      post '/stripe_events', params: payload
      expect(alice.reload).not_to be_active
    end
  end
end
