class ForgotPasswordsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user
      ApplicationMailer.send_forgot_password(user).deliver_later(wait: 10.seconds)
      redirect_to forgot_password_confirmation_path
    else
      flash[:danger] = params[:email].blank? ? "Email cannot ba blank" : "No user with the email is found"
      redirect_to forgot_password_path
    end
  end

  def confirm
  end
end