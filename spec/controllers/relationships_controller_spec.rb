require 'spec_helper'
require 'rails_helper'

describe RelationshipsController do
  describe "GET index" do
    it "sets @relationships to the current user's following relationships" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      relationship1 = Fabricate(:relationship, follower: alice, leader: bob)
      get :index
      expect(assigns(:relationships)).to eq([relationship1])
    end

    it_behaves_like "require log in" do
      let(:action) { get :index }
    end
  end

  describe "DELETE destroy" do
    it_behaves_like "require log in" do
      let(:action) { delete :destroy, params: { id: 4 } }
    end

    it "redirects to people page" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      relationship = Fabricate(:relationship, follower: alice, leader: bob)
      delete :destroy, params: { id: relationship.id }
      expect(response).to redirect_to people_path
    end

    it "deletes relationship if current user is follower" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      relationship = Fabricate(:relationship, follower: alice, leader: bob)
      delete :destroy, params: { id: relationship.id }
      expect(Relationship.count).to eq(0)
    end

    it "not deletes relationship if current user is not follower" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      relationship = Fabricate(:relationship, follower: bob, leader: alice)
      delete :destroy, params: { id: relationship.id }
      expect(Relationship.count).to eq(1)
    end
  end

  describe "POST create" do
    it_behaves_like "require log in" do
      let(:action) { post :create, params: { leader_id: 2 } }
    end

    it "redirects to people path" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      post :create, params: { leader_id: bob.id }
      expect(response).to redirect_to people_path
    end

    it "creates a relationship with current user as follower" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      post :create, params: { leader_id: bob.id }
      expect(alice.following_relationships.first.leader).to eq(bob)
    end

    it "not creates a relationship if current user already folllows follower" do
      alice = Fabricate(:user)
      set_current_user(alice)
      bob = Fabricate(:user)
      Fabricate(:relationship, leader: bob, follower: alice)
      post :create, params: { leader_id: bob.id }
      expect(alice.following_relationships.first.leader).to eq(bob)
    end

    it "cannot follow self" do
      alice = Fabricate(:user)
      set_current_user(alice)
      post :create, params: { leader_id: alice.id }
      expect(Relationship.count).to eq(0)
    end
  end
end