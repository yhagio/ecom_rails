require 'spec_helper'
require 'rails_helper'

describe "Stripe Webhook Events" do
  def bypass_event_signature(payload)
    event = Stripe::Event.construct_from(JSON.parse(payload, symbolize_names: true))
    expect(Stripe::Webhook).to receive(:construct_event).and_return(event)
  end

  describe "charge.succeeded event" do
    let!(:alice) { Fabricate(:user, customer_token: "cus_00000000000000") }
    let(:payload) { File.read("spec/fixtures/stripe_charge_succeeded.json") }
    before(:each) { bypass_event_signature payload }

    it "is successful" do
      post '/stripe_events', params: payload
      expect(response.code).to eq "200"
    end

    it "creates a payment", :vcr do
      post "/stripe_events", params: payload
      expect(Payment.count).to eq(1)
    end

    it "creates a payment with associated to user",:vcr do
      post "/stripe_events", params: payload
      expect(Payment.first.user).to eq(alice)
    end

    it "creates a payment with amount",:vcr do
      post "/stripe_events", params: payload
      expect(Payment.first.amount).to eq(999)
    end
  end
end