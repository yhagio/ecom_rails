require 'spec_helper'
require 'rails_helper'

describe PasswordResetsController do
  describe "GET show" do

    it "renders template if token is valid" do
      alice = Fabricate(:user)
      alice.update_column(:token, 'abc123')
      get :show, params: { id: 'abc123' }
      expect(response).to render_template :show
    end

    it "sets token" do
      alice = Fabricate(:user)
      alice.update_column(:token, 'abc123')
      get :show, params: { id: 'abc123' }
      expect(assigns(:token)).to eq('abc123')
    end

    it "redirects to the expired token page if token is expired" do
      get :show, params: { id: 'abc123' }
      expect(response).to redirect_to expired_token_path
    end

  end

  describe "POST create" do
    context "with valid token" do
      it "updates user password" do
        alice = Fabricate(:user, password: 'old_pass')
        alice.update_column(:token, 'abc123')
        post :create, params: { password: 'new_pass', token: 'abc123' }
        expect(alice.reload.authenticate('new_pass')).to be_truthy
      end

      it "redirects to login page" do
        alice = Fabricate(:user, password: 'old_pass')
        alice.update_column(:token, 'abc123')
        post :create, params: { password: 'new_pass', token: 'abc123' }
        expect(response).to redirect_to login_path
      end

      it "sets flash success" do
        alice = Fabricate(:user, password: 'old_pass')
        alice.update_column(:token, 'abc123')
        post :create, params: { password: 'new_pass', token: 'abc123' }
        expect(flash[:success]).to be_present
      end

      it "regenerates user token" do
        alice = Fabricate(:user, password: 'old_pass')
        alice.update_column(:token, 'abc123')
        post :create, params: { password: 'new_pass', token: 'abc123' }
        expect(alice.reload.token).not_to eq('abc123')
      end
    end

    context "with invalid token" do
      it "redirects to the expired token page if token is expired" do
        post :create, params: { password: 'new_pass', token: 'abc123' }
        expect(response).to redirect_to expired_token_path
      end
    end
  end
end