require 'spec_helper'
require 'rails_helper'

describe ReviewsController do
  describe "POST create" do
    context "With authed users" do
      let(:user) { Fabricate(:user) }
      let(:video) { Fabricate(:video) }

      before do
        session[:user_id] = user.id
      end

      context "With valid input" do
        before do
          post :create, params: { review: Fabricate.attributes_for(:review), video_id: video.id, user_id: user.id }
        end

        it "creates a review" do
          expect(Review.count).to eq(1)
        end

        it "creates a review associated with the video" do
          expect(Review.first.video).to eq(video)
        end

        it "creates a review associated with the logged in user" do
          expect(Review.first.user).to eq(user)
        end

        it "redirects to the video show page" do
          expect(response).to redirect_to video
        end
      end

      context "With invalid input" do
        it "does not create a review" do
          post :create, params: { review: { rating: 2 }, video_id: video.id, user_id: user.id }
          expect(Review.count).to eq(0)
        end

        it "renders the videos/show template" do
          post :create, params: { review: { rating: 2 }, video_id: video.id, user_id: user.id }
          expect(response).to render_template "videos/show"
        end

        it "sets @video" do
          post :create, params: { review: { rating: 2 }, video_id: video.id, user_id: user.id }
          expect(assigns(:video)).to eq(video)
        end

        it "sets @reviews" do
          review = Fabricate(:review, video: video, user: user)
          post :create, params: { review:{ rating: 2 }, video_id: video.id, user_id: user.id }
          expect(assigns(:reviews)).to match_array([review])
        end
      end
    end

    context "With unsuthed users" do
      it "redirects to login" do
        post :create, params: { review: Fabricate.attributes_for(:review), video_id: "test" }
        expect(response).to redirect_to login_path
      end
    end
  end
end