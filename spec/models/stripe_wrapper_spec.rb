require 'spec_helper'
require 'rails_helper'

describe StripeWrapper do
  let(:valid_token) do
    Stripe::Token.create({
      card: {
        number: '4242424242424242',
        exp_month: 5,
        exp_year: 2020,
        cvc: '314',
      },
    }).id
  end

  let(:valid_customer_id) do
    Stripe::Customer.create({
      email: 'test@cc.cc',
      source: valid_token
    }).id
  end

  describe StripeWrapper::Charge do
    describe ".create" do
      it "makes a successful charge", :vcr do
        res = StripeWrapper::Charge.create({
          amount: 2000,
          currency: 'cad',
          source: valid_token,
          description: 'Charge for test',
        })

        expect(res).to be_successful
      end

      it "makes a failed charge", :vcr do
        res = StripeWrapper::Charge.create({
          amount: 2000,
          currency: 'cad',
          source: 'invalid_token',
          description: 'Charge for test',
        })
        expect(res).not_to be_successful
        expect(res.error_message).to be_present
      end
    end
  end

  describe StripeWrapper::Customer do
    it "creates a customer with valid card", :vcr do
      res = StripeWrapper::Customer.create({
        email: "alice@cc.cc",
        source: valid_token
      })

      expect(res).to be_successful
    end

    it "returns customer token (id) for valid card", :vcr do
      res = StripeWrapper::Customer.create({
        email: "alice@cc.cc",
        source: valid_token
      })

      expect(res.customer_token).to be_present
    end

    it "does not create a customer with invalid card", :vcr do
      res = StripeWrapper::Customer.create({
        email: "alice@cc.cc",
        source: 'invalid_token'
      })
      expect(res).not_to be_successful
      expect(res.error_message).to be_present
    end

    it "deletes a customer with valid customer id", :vcr do
      res = StripeWrapper::Customer.delete(valid_customer_id)
      expect(res).to be_successful
    end

    it "does not delete a customer with invalid customer id", :vcr do
      res = StripeWrapper::Customer.delete('valid_customer_id')
      expect(res).not_to be_successful
      expect(res.error_message).to be_present
    end
  end

  describe StripeWrapper::Subscription do
    it "creates a subscription with valid card", :vcr do
      res = StripeWrapper::Subscription.create({
        customer: valid_customer_id
      })

      expect(res).to be_successful
    end

    it "does not create a subscription with invalid customer id", :vcr do
      res = StripeWrapper::Subscription.create({
        customer: 'valid_customer_id'
      })
      expect(res).not_to be_successful
      expect(res.error_message).to be_present
    end
  end
end