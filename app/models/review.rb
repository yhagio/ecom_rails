class Review < ActiveRecord::Base
  default_scope { order('created_at DESC') }

  belongs_to :video, touch: true # updates video when review is updated
  belongs_to :user

  validates_presence_of :content, :rating
end