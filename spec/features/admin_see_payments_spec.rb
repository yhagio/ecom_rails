require 'spec_helper'
require 'rails_helper'

feature 'Admin sees payments' do
  background do
    bob = Fabricate(:user, full_name: "Bob Smith", email: "bob@cc.cc")
    Fabricate(:payment, user_id: bob.id, amount: 999, reference_id: 'ref_id')
  end

  scenario "Admin can view payments" do
    admin_user = Fabricate(:admin)
    user_login(admin_user)
    visit admin_payments_path
    expect(page).to have_content "CAD 9.99"
    expect(page).to have_content "Bob Smith"
    expect(page).to have_content "bob@cc.cc"
  end

  scenario "Non admin user can not see payments" do
    user_login
    visit admin_payments_path
    expect(page).not_to have_content "CAD 9.99"
    expect(page).to have_content "You are not authorized to do that"
  end
end