require 'spec_helper'
require 'rails_helper'

feature "user resets password" do
  scenario "user successfully resets password" do
    alice = Fabricate(:user, password: 'old_pass')
    visit login_path
    click_link "Forgot Password?"
    fill_in "Email Address", with: alice.email
    click_button "Send Email"

    open_email(alice.email)
    current_email.click_link("Reset My Password")

    fill_in "New Password", with: "new_pass"
    click_button "Reset Password"

    fill_in "Email Address", with: alice.email
    fill_in "Password", with: "new_pass"
    click_button "Sign in"

    expect(page).to have_content("Welcome, #{alice.full_name}")

    # clear_email
  end
end
