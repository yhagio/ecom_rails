require 'spec_helper'
require 'rails_helper'

describe ForgotPasswordsController do
  describe "POST create" do
    context "with blank email" do
      before do
        post :create, params: { email: '' }
      end
      it "redirects to forgot password page" do
        expect(response).to redirect_to forgot_password_path
      end
      it "sets flash error" do
        expect(flash[:danger]).to eq("Email cannot ba blank")
      end
    end

    context "with non-existing email" do
      before do
        user = Fabricate(:user, email: 'testtest@cc.cc')
        post :create, params: { email: user.email }
      end
      it "redirects to forgot password page" do
        expect(response).to redirect_to forgot_password_confirmation_path
      end
      it "sends out email" do
        expect(ActionMailer::Base.deliveries.last.to).to eq(['testtest@cc.cc'])
      end
    end

    context "with existing email" do
      before do
        post :create, params: { email: "test@cc.cc" }
      end
      it "redirects to forgot password page" do
        expect(response).to redirect_to forgot_password_path
      end
      it "sets flash error" do
        expect(flash[:danger]).to eq("No user with the email is found")
      end
    end
  end
end