# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Video.destroy_all
Category.destroy_all
VideoCategory.destroy_all
Review.destroy_all
User.destroy_all
QueueItem.destroy_all
Relationship.destroy_all

Category.create(name: 'Action')
Category.create(name: 'Comedy')
Category.create(name: 'Drama')

sample = Video.create(title: 'Sample1', description: 'Sample example description 1', small_cover: open('public/tmp/monk.jpg'), large_cover: open('public/tmp/monk_large.jpg'))
sample2 = Video.create(title: 'Sample2', description: 'Sample example description 2', small_cover: open('public/tmp/monk.jpg'), large_cover: open('public/tmp/monk_large.jpg'))
sample3 = Video.create(title: 'Sample3', description: 'Sample example description 3', small_cover: open('public/tmp/monk.jpg'), large_cover: open('public/tmp/monk_large.jpg'))
Video.create(title: 'Sample4', description: 'Sample example description 4', small_cover: open('public/tmp/monk.jpg'), large_cover: open('public/tmp/monk_large.jpg'))
Video.create(title: 'Sample5', description: 'Sample example description 5', small_cover: open('public/tmp/monk.jpg'), large_cover: open('public/tmp/monk_large.jpg'))
Video.create(title: 'Sample6', description: 'Sample example description 6', small_cover: open('public/tmp/monk.jpg'), large_cover: open('public/tmp/monk_large.jpg'))

VideoCategory.create(video_id: Video.find_by(title: 'Sample1').id, category_id: Category.find_by(name: 'Action').id)
VideoCategory.create(video_id: Video.find_by(title: 'Sample2').id, category_id: Category.find_by(name: 'Comedy').id)
VideoCategory.create(video_id: Video.find_by(title: 'Sample3').id, category_id: Category.find_by(name: 'Drama').id)
VideoCategory.create(video_id: Video.find_by(title: 'Sample4').id, category_id: Category.find_by(name: 'Comedy').id)
VideoCategory.create(video_id: Video.find_by(title: 'Sample5').id, category_id: Category.find_by(name: 'Action').id)
VideoCategory.create(video_id: Video.find_by(title: 'Sample6').id, category_id: Category.find_by(name: 'Drama').id)

alice = User.create(full_name: "Alice Smith", password: "123test", email: "alice@cc.cc")
alice.update_column(:admin, true)
bob = User.create(full_name: "Bob Smith", password: "123test", email: "bob@cc.cc")
chris = User.create(full_name: "Chris Smith", password: "123test", email: "chris@cc.cc")

Review.create(user: alice, video: sample, rating: 3, content: "This was a OK movie I guess")
Review.create(user: bob, video: sample, rating: 4, content: "This was a good movie I guess")
Review.create(user: chris, video: sample, rating: 5, content: "This was a great movie I guess")

QueueItem.create(user: alice, video: sample, position: 1)
QueueItem.create(user: alice, video: sample2, position: 2)
QueueItem.create(user: alice, video: sample3, position: 3)

Relationship.create(leader_id: bob.id, follower_id: alice.id)
Relationship.create(leader_id: chris.id, follower_id: alice.id)
Relationship.create(leader_id: alice.id, follower_id: bob.id)
Relationship.create(leader_id: bob.id, follower_id: chris.id)
