require 'spec_helper'
require 'rails_helper'

describe UsersController do
  describe "GET new" do
    it "sets @user" do
      get :new
      expect(assigns(:user)).to be_instance_of(User)
    end

    it "redirects to home if already logged in" do
      session[:user_id] = Fabricate(:user).id
      get :new
      expect(response).to redirect_to home_path
    end
  end

  describe "POST create" do
    context "With valid personal inputs and valid credit card information" do
      before do
        result = double(:result, successful?: true )
        expect_any_instance_of(UserRegistration).to receive(:register).and_return(result)
        post :create, params: { stripeToken: "token", user: Fabricate.attributes_for(:user) }
      end

      it "redirects to login page" do
        expect(response).to redirect_to login_path
      end

      it "sets flash success" do
        expect(flash[:success]).to be_present
      end
    end

    context "With invalid personal inputs" do
      before do
        result = double(:result, successful?: false, error_message: "Nop")
        expect_any_instance_of(UserRegistration).to receive(:register).and_return(result)
        post :create, params: { stripeToken: "token", user: Fabricate.attributes_for(:user) }
      end

      it "render the :new template" do
        expect(response).to render_template :new
      end

      it "sets @user" do
        expect(assigns(:user)).to be_instance_of(User)
      end

      it "sets flash danger" do
        expect(flash[:danger]).to be_present
      end
    end
  end

  describe "GET show" do
    it_behaves_like "require log in" do
      let(:action) { get :show, params: { id: 3 } }
    end

    it "sets @user" do
      set_current_user
      alice = Fabricate(:user)
      get :show, params: { id: alice.id }
      expect(assigns(:user)).to eq(alice)
    end
  end

  describe "GET new_with_invitation_token" do
    let(:alice) { Fabricate(:user) }
    let(:invitation) { Fabricate(:invitation, inviter: alice) }

    context "with valid token" do
      before do
        get :new_with_invitation_token, params: { token: invitation.token }
      end

      it "renders new template" do
        expect(response).to render_template :new
      end

      it "sets @user with recipient's name" do
        expect(assigns(:user).email).to eq(invitation.recipient_email)
      end

      it "sets @invitation_token" do
        expect(assigns(:invitation_token)).to eq(invitation.token)
      end
    end

    it "redirects to expired token page if token is invalid" do
      get :new_with_invitation_token, params: { token: "123" }
      expect(response).to redirect_to expired_token_path
    end
  end
end