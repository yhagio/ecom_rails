require 'spec_helper'
require 'rails_helper'

describe VideosController do
  describe "GET show" do
    let(:video) { Fabricate(:video) }
    let(:review1) { Fabricate(:review, video: video, user: Fabricate(:user)) }
    let(:review2) { Fabricate(:review, video: video, user: Fabricate(:user)) }

    context "With authenticated user" do
      before do
        session[:user_id] = Fabricate(:user).id
        get :show, params: { id: video.id }
      end

      it "sets @video" do
        expect(assigns(:video)).to eq(video)
      end

      it "sets @reviews" do
        expect(assigns(:reviews)).to match_array([review1, review2])
      end

      # ---- No need to test rendering template since it is Rails job, not mine
      # it "renders the show template" do
      #   get :show, params: { id: video.id }
      #   expect(response).to render_template :show
      # end
    end

    context "with unauthenticated users" do
      it "redirects the user to the log in page" do
        get :show, params: { id: video.id }
        expect(response).to redirect_to login_path
      end
    end
  end

  describe "POST search" do
    let(:video) { Fabricate(:video, { title: "mytest123" }) }

    context "With authenticated user" do
      before do
        session[:user_id] = Fabricate(:user).id
      end

      it "sets @videos" do
        post :search, params: { term: "test" }
        expect(assigns(:videos)).to eq([video])
      end
    end

    context "with unauthenticated users" do
      it "redirects the user to the log in page" do
        post :search, params: { term: "test" }
        expect(response).to redirect_to login_path
      end
    end
  end
end
