require 'spec_helper'
require 'rails_helper'

feature "user following" do
  scenario "user follows and unfollows another user" do
    alice = Fabricate(:user)
    category = Fabricate(:category)
    video = Fabricate(:video)
    Fabricate(:video_category, video_id: video.id, category_id: category.id)
    Fabricate(:review, user: alice, video: video)

    user_login
    click_on_video_on_home_page(video)

    click_link alice.full_name
    click_link "Follow"
    expect(page).to have_content(alice.full_name)

    unfollow(alice)
    expect(page).not_to have_content(alice.full_name)
  end

  def unfollow(user)
    find("a[data-method='delete']").click
  end
end