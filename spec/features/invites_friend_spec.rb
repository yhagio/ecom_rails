# require 'spec_helper'
# require 'rails_helper'

# feature "User invites a friend" do
#   scenario "User successfully invites a friend and the invitation is accepted", { js: true, vcr: { record: :all } } do
#     # clear_emails

#     alice = Fabricate(:user)
#     friend = Fabricate.attributes_for(:user)
#     user_login(alice)
#     invite_a_friend

#     friend_accepts_invitation
#     friend_logs_in
#     friend_should_follow(alice)
#     inviter_should_follow_friend(alice)
#   end

#   def invite_a_friend
#     visit new_invitation_path
#     fill_in "Friend's Name", with: "John Doe"
#     fill_in "Friend's Email Address", with: "john@example.com"
#     fill_in "Invitation Message", with: "Join me"

#     click_button "Send Invitation"
#     expect(page).to have_selector('.alert.alert-success')
#     expect(page).to have_content "You successfully invited John Doe"
#     user_logout
#   end

#   def friend_accepts_invitation
#     open_email("john@example.com")
#     current_email.click_link "Accept this invitation"
#     fill_in "Password", with: "password"
#     fill_in "Full Name", with: "John Doe"

#     fill_in name: "cardnumber", with: "4242424242424242"
#     fill_in name: "exp-date", with: "12/30"
#     fill_in name: "cvc", with: "123"
#     fill_in name: "postal", with: "12345"

#     click_button "Sign Up"
#   end

#   def friend_logs_in
#     fill_in "Email Address", with: "john@example.com"
#     fill_in "Password", with: "password"
#     # save_and_open_page
#     click_button "Login"
#   end

#   def friend_should_follow(user)
#     click_link "People"
#     expect(page).to have_content user.full_name
#     user_logout
#   end

#   def inviter_should_follow_friend(user)
#     user_login(user)
#     click_link "People"
#     expect(page).to have_content "John Doe"
#   end
# end