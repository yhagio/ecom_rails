#!/bin/bash

build() {
  docker-compose build
  # docker-compose -f docker-compose.app.yml build
}

start() {
  docker-compose up
}

stop() {
  docker-compose down
}

test() {
  docker-compose exec -e"RAILS_ENV=test" ecom_rails_web rspec
  # docker-compose -f docker-compose.app.yml exec -e"RAILS_ENV=test" ecom_rails_web rspec
}

seed() {
  docker-compose exec -e"RAILS_ENV=development" ecom_rails_web rake db:seed
  # docker-compose -f docker-compose.app.yml exec -e"RAILS_ENV=development" ecom_rails_web rake db:seed
}

migrate() {
  docker-compose exec -e"RAILS_ENV=development" ecom_rails_web rake db:migrate db:test:prepare
  # docker-compose -f docker-compose.app.yml exec -e"RAILS_ENV=development" ecom_rails_web rake db:migrate db:test:prepare
}

rollback() {
  docker-compose exec -e"RAILS_ENV=development" ecom_rails_web rake db:rollback db:test:prepare
  # docker-compose -f docker-compose.app.yml exec -e"RAILS_ENV=development" ecom_rails_web rake db:rollback db:test:prepare
}

install() {
  docker-compose exec ecom_rails_web bundle install
  # docker-compose -f docker-compose.app.yml exec ecom_rails_web bundle install
}

in_web() {
  docker exec -it ecom_rails_web sh
}