class Payment < ActiveRecord::Base
  default_scope { order('created_at DESC') }

  belongs_to :user

  validates_presence_of :user_id, :amount, :reference_id
end
