require 'spec_helper'
require 'rails_helper'

describe Admin::VideosController do
  describe "GET new" do
    it_behaves_like "require log in" do
      let(:action) { get :new }
    end

    it "sets @video" do
      set_current_admin
      get :new
      expect(assigns(:video)).to be_new_record
      expect(assigns(:video)).to be_instance_of Video
    end

    it_behaves_like "requires admin" do
      let(:action) { get :new }
    end

    it "sets flash danger for non-admin users" do
      set_current_user
      get :new
      expect(flash[:danger]).to be_present
    end
  end

  describe "POST create" do
    it_behaves_like "require log in" do
      let(:action) { post :create }
    end

    it_behaves_like "requires admin" do
      let(:action) { post :create }
    end

    context "with valid inputs" do
      before do
        set_current_admin
      end

      let(:category) { Fabricate(:category) }

      it "creates the video" do
        post :create, params: { video: { title: "Batman", category_ids: [category], description: "A great superhero movie" } }
        expect(category.videos.count).to eq(1)
      end
      it "sets flash success" do
        post :create, params: { video: { title: "Batman", category_ids: [category], description: "A great superhero movie" } }
        expect(flash[:success]).to be_present
      end
      it "redirects the add video page" do
        post :create, params: { video: { title: "Batman", category_ids: [category], description: "A great superhero movie" } }
        expect(response).to redirect_to new_admin_video_path
      end
    end

    context "with invalid inputs" do
      before do
        set_current_admin
      end

      let(:category) { Fabricate(:category) }

      it "not create the video" do
        post :create, params: { video: { category_ids: [category], description: "A great superhero movie" } }
        expect(category.videos.count).to eq(0)
      end
      it "renders the new template" do
        post :create, params: { video: { category_ids: [category], description: "A great superhero movie" } }
        expect(response).to render_template :new
      end
      it "sets @video" do
        post :create, params: { video: { category_ids: [category], description: "A great superhero movie" } }
        expect(assigns(:video)).to be_present
      end
      it "sets flash danger" do
        post :create, params: { video: { category_ids: [category], description: "A great superhero movie" } }
        expect(flash[:danger]).to be_present
      end
    end
  end
end