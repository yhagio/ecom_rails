require 'spec_helper'
require 'rails_helper'

describe QueueItemsController do
  describe "GET index" do
    it "sets @queue_items to the queue items of the logged in user" do
      alice = Fabricate(:user)
      set_current_user(alice)
      queue_item1 = Fabricate(:queue_item, user: alice, video: Fabricate(:video))
      queue_item2 = Fabricate(:queue_item, user: alice, video: Fabricate(:video))
      get :index
      expect(assigns(:queue_items)).to match_array([queue_item2, queue_item1])
    end

    # it "redirects to login if not authenticated" do
    #   get :index
    #   expect(response).to redirect_to login_path
    # end

    it_behaves_like "require log in" do
      let(:action) { get :index }
    end
  end

  describe "POST create" do
    context "with authenticated user" do
      let(:user) { Fabricate(:user) }
      let(:video) { Fabricate(:video) }
      before do
        session[:user_id] = user.id
      end

      it "redirects to the my queue page" do
        post :create, params: { video_id: video.id }
        expect(response).to redirect_to my_queue_path
      end

      it "creates a queue item" do
        post :create, params: { video_id: video.id }
        expect(QueueItem.count).to eq(1)
      end

      it "creates the queue item that is associated with the video" do
        post :create, params: { video_id: video.id }
        expect(QueueItem.first.video).to eq(video)
      end

      it "creates the queue item that is associated with logged in user" do
        post :create, params: { video_id: video.id }
        expect(QueueItem.first.user).to eq(user)
      end

      it "puts the video in the last queue item" do
        Fabricate(:queue_item, video: Fabricate(:video, video_url: 'blah.com'), user: user)
        post :create, params: { video_id: video.id }
        vid = QueueItem.where(video_id: video.id, user_id: user.id).first
        expect(vid.position).to eq(2)
      end

      it "does not add the video if already in the queue" do
        Fabricate(:queue_item, video: video, user: user)
        post :create, params: { video_id: video.id }
        expect(QueueItem.count).to eq(1)
      end
    end

    context "without logged in" do
      it "redirects to login" do
        post :create, params: { video_id: Fabricate(:video).id }
        expect(response).to redirect_to login_path
      end
    end
  end

  describe "DELETE destroy" do

    context "with authenticated user" do
      let(:user) { Fabricate(:user) }
      let(:video) { Fabricate(:video) }
      let(:my_queue) { Fabricate(:queue_item, video: video, user: user, position: 1) }
      before do
        session[:user_id] = user.id
      end

      it "redirects to my queue" do
        delete :destroy, params: { id: my_queue.id }
        expect(response).to redirect_to my_queue_path
      end

      it "deletes the queue" do
        delete :destroy, params: { id: my_queue.id }
        expect(QueueItem.count).to eq(0)
      end

      it "does not delete the queue if it is not the logged in user's" do
        bob_queue = Fabricate(:queue_item, video: Fabricate(:video), user: Fabricate(:user))
        delete :destroy, params: { id: bob_queue.id }
        expect(QueueItem.count).to eq(1)
      end

      it "normalizes the remaining item orders" do
        queue2 = Fabricate(:queue_item, video: Fabricate(:video), user: user, position: 2)
        queue3 = Fabricate(:queue_item, video: Fabricate(:video), user: user, position: 3)
        delete :destroy, params: { id: my_queue.id }
        expect(queue2.reload.position).to eq(1)
        expect(queue3.reload.position).to eq(2)
      end
    end

    context "without logged in" do
      it "redirects to login" do
        delete :destroy, params: { id: 1 }
        expect(response).to redirect_to login_path
      end
    end
  end

  describe "POST update_queue" do
    let(:alice) { Fabricate(:user) }
    let(:queue1) { Fabricate(:queue_item, video: Fabricate(:video, video_url: 'etettet.com'), user: alice, position: 1) }
    let(:queue2) { Fabricate(:queue_item, video: Fabricate(:video, video_url: 'tests.com'), user: alice, position: 2) }
    context "with valid inputs" do
      it "redirects to my queue page" do
        set_current_user(alice)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 2}, {id: queue2.id, position: 1}]}
        expect(response).to redirect_to my_queue_path
      end

      it "reorders the queue items" do
        set_current_user(alice)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 2}, {id: queue2.id, position: 1}]}
        expect(alice.queue_items).to eq([queue2, queue1])
      end

      it "normalizes the position numbers" do
        set_current_user(alice)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 3}, {id: queue2.id, position: 2}]}
        expect(alice.queue_items.map(&:position)).to eq([1, 2])
        expect(queue2.reload.position).to eq(1)
        expect(queue1.reload.position).to eq(2)
      end
    end

    context "with invalid inputs" do
      it "redirects to my queue page" do
        set_current_user(alice)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 3}, {id: queue2.id, position: 2.1}]}
        expect(response).to redirect_to my_queue_path
      end

      it "does not change records" do
        set_current_user(alice)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 3}, {id: queue2.id, position: 0.5}]}
        expect(queue1.reload.position).to eq(1)
        expect(queue2.reload.position).to eq(2)
      end

      it "sets error flash" do
        set_current_user(alice)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 4}, {id: queue2.id, position: 1.4}]}
        expect(flash[:danger]).not_to be_blank
      end
    end

    context "with queue items of other users" do
      it "does not change the queue items" do
        bob = Fabricate(:user)
        set_current_user(alice)
        video = Fabricate(:video)
        queue1 = Fabricate(:queue_item, video: Fabricate(:video), user: bob, position: 1)
        queue2 = Fabricate(:queue_item, video: Fabricate(:video), user: bob, position: 2)
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 2}, {id: queue2.id, position: 1}]}
        expect(queue1.reload.position).to eq(1)
        expect(queue2.reload.position).to eq(2)
      end
    end

    context "with unauthed user" do
      it "redirects to login" do
        post :update_queue, params: { queue_items: [{id: queue1.id, position: 2}, {id: queue2.id, position: 1}]}
        expect(response).to redirect_to login_path
      end
    end
  end
end