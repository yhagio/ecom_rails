# ECOM RAILS

- ECommerce Rails App

## How to run locally

```sh
rvm use default # 2.6.0
bundle install
```

**There's script file using Docker**

```sh
source commands.sh

# To setup and start the stack
build         # 1 Build application
start         # 2 Start Rails application in another terminal

# Go to http://localhost:3000
# You can see Sidekiq is running at
# http://localhost:3000/sidekiq (Takes care of sending emails)
# http://localhost:9200/ (Elasticsearch)

# Other commands
stop          # Stop Rails application gracefully
seed          # Seed data
migrate       # Migrate db and prepare for test
rollback      # Rollback the migration
test          # Run tests (rspec)
```


---

**Without Docker**

```sh
rake db:seed                    # If you want to seed data

bundle exec sidekiq -q mailers  # Start Sidekiq
bundle exec puma                # Run Rails app
# Go to localhost:3000
```

Run migration, rspec test
```sh
rake db:migrate db:test:prepare # Every time migarated
rspec                           # Run whole tests
```

**With Docker**

Start
```sh
# Start application (Redis, Postgres, Rails, etc)
docker-compose up
# Go to localhost:3000
```


Run migration, rspec test
```
docker-compose exec ecom_rails_web rake db:seed
docker-compose exec ecom_rails_web rake db:migrate db:test:prepare

docker-compose exec -e"RAILS_ENV=test" ecom_rails_web rspec
```

Environmental variables
```sh
- RAILS_ENV=development
- APP_HOST=localhost
- PORT=3000
- DB_HOST=ecom_rails_db
- DB_USER=dev_user
- DB_PASSWORD=123test
- REDIS_URL=redis://ecom_rails_redis:6379
- MAILGUN_DOMAIN=
- MAILGUN_SECRET_KEY=
```

---
- [x] Docker / docker-compose
- [x] Redis / Postgres
- [x] Puma
- [x] Mailgun
- [x] Sidekiq
- [x] Stripe (subscription)
- [x] Elasticsearch
- [x] Plan and billing
- [x] Cancel subscription
- [x] Account page (uer profile + update)

- [ ] CI
- [ ] CD
- [ ] Deploy to Heroku / Digital Ocean
- [ ] (AWS S3 or cloudinary) for image upload
- [ ] Customized UI (Stripe credit card as well)
---
When installed rspec and setup boilerplate
```sh
rails generate rspec:install
```

#### Use Stripe webhook locally

- Install ngrok
- Update Stripe webhook endpoint from dashboard

```
./ngrok http 3000
```


### References

- [Sidekiq-Action Mailer](https://github.com/mperham/sidekiq/wiki/Active-Job#action-mailer)
- [Carrierwave](https://github.com/carrierwaveuploader/carrierwave)
- [MiniMagick](https://github.com/minimagick/minimagick)
- [Dockerizing Rails](https://nickjanetakis.com/blog/dockerize-a-rails-5-postgres-redis-sidekiq-action-cable-app-with-docker-compose)

**Stripe**

- [Stripe custom form using elements (UI)](https://stripe.com/docs/stripe-js/elements/migrating)
- [Stripe charges card (Server)](https://stripe.com/docs/charges)
- [Stripe testing card numbers](https://stripe.com/docs/testing)
- [Stripe API](https://stripe.com/docs/api)

---
#### Known issues

- Commented out feature test that requires Javascript with Capybara (with VCR, Selenium)

- (Locally) If you see following error

```
max virtual memory areas vm.max_map_count [65530] likely too low, increase to at least [262144]
```
Do this
```
sudo sysctl -w vm.max_map_count=262144
```