class User < ActiveRecord::Base
  include Tokenable

  validates_presence_of :email, :full_name, :password
  validates_uniqueness_of :email

  has_many :reviews
  has_many :queue_items
  has_many :following_relationships, class_name: "Relationship", foreign_key: :follower_id
  has_many :leading_relationships, class_name: "Relationship", foreign_key: :leader_id

  has_secure_password validations: false

  def normalize_queue_items
    queue_items.each_with_index do |item, index|
      item.update_attributes(position: index+1)
    end
  end

  def queued_video?(video)
    queue_items.map(&:video).include?(video)
  end

  def follows?(a_user)
    following_relationships.map(&:leader).include?(a_user)
  end

  def can_follow?(a_user)
    !(self.follows?(a_user) || self == a_user)
  end

  def follow(a_user)
    following_relationships.create(leader: a_user) if can_follow?(a_user)
  end

  def deactivate!
    update_column(:active, false)
  end
end