Fabricator(:video) do
  title { Faker::Lorem.words(5).join(' ') }
  description { Faker::Lorem.paragraph(2) }
  video_url { Faker::Lorem.words(3).join('') }
end