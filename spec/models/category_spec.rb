require 'spec_helper'
require 'rails_helper'
describe Category do
  it { should have_many(:videos) }
  it { should validate_presence_of(:name) }

  it "saves itself" do
    drama = Category.new(name: "drama")
    drama.save
    expect(Category.first.name).to eql('drama')
  end

  it "does not save if no name is given" do
    Category.create(name: "")
    expect(Category.count).to eq(0)
  end


  it "has a video" do
    drama = Category.create(name: "drama")
    video = Video.create(title: 'test123', description: 'test desc123', video_url: 'some_url')
    VideoCategory.create(video_id: video.id, category_id: drama.id)
    expect(drama.videos.first).to eql(video)
  end

  describe "#recent_videos" do
    it "returns the videos in the reverse chronical order by created at" do
      drama = Category.create(name: "drama")

      video1 = Video.create(title: 'test123', description: 'test desc123', video_url: 'some_url')
      video2 = Video.create(title: 'test124', description: 'test desc124', video_url: 'some_url2')
      video3 = Video.create(title: 'test125', description: 'test desc125', video_url: 'some_url3')

      VideoCategory.create(video_id: video1.id, category_id: drama.id)
      VideoCategory.create(video_id: video2.id, category_id: drama.id)
      VideoCategory.create(video_id: video3.id, category_id: drama.id)

      expect(drama.recent_videos).to eq([video3, video2, video1])
    end

    it "returns 6 videos if there are more than 6 videos" do
      drama = Category.create(name: "drama")

      (1..10).each do |i|
        video = Video.create(title: "test#{i}", description: "test desc#{i}", video_url: "url#{i}")
        VideoCategory.create(video_id: video.id, category_id: drama.id)
      end

      expect(drama.recent_videos.count).to eq(6)
    end

    it "returns the most recent 6 videos" do
      drama = Category.create(name: "drama")

      (1..10).each do |i|
        video = Video.create(title: "test#{i}", description: "test desc#{i}", video_url: "url#{i}")
        VideoCategory.create(video_id: video.id, category_id: drama.id)
      end

      expect(drama.recent_videos.count).to eq(6)
      expect(drama.recent_videos.first.title).to eq("test10")
      expect(drama.recent_videos.last.title).to eq("test5")

    end

    it "returns an empty array if the category does not have any videos" do
      drama = Category.create(name: "drama")
      expect(drama.recent_videos.count).to eq(0)
    end
  end
end
