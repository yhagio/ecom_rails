class QueueItemsController < ApplicationController
  before_action :require_user

  def index
    @queue_items = current_user.queue_items
  end

  def create
    video = Video.find(params[:video_id])
    queue_video(video)
    redirect_to my_queue_path
  end

  def update_queue
    begin
      update_queue_items
      current_user.normalize_queue_items
    rescue
      flash[:danger] = "Invalid position numbers"
    end

    redirect_to my_queue_path
  end

  def destroy
    queue = QueueItem.find(params[:id])
    queue.destroy if user_owns_the_queue?(queue)
    current_user.normalize_queue_items
    redirect_to my_queue_path
  end

  private

  def new_queue_position
    current_user.queue_items.count + 1
  end

  def is_queued_already?(video)
    current_user.queue_items.map(&:video).include?(video)
  end

  def queue_video(video)
    QueueItem.create(video: video, user: current_user, position: new_queue_position) unless is_queued_already?(video)
  end

  def user_owns_the_queue?(queue)
    current_user.queue_items.include?(queue)
  end

  def update_queue_items
    ActiveRecord::Base.transaction do
      params[:queue_items].each do |item|
        queue = QueueItem.find(item['id'])
        queue.update_attributes!(position: item['position'], rating: item['rating']) if queue.user == current_user
      end
    end
  end
end