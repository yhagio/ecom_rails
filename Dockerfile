FROM ruby:2.6.0

RUN apt-get update && apt-get install -y \
  nodejs \
  libmagickwand-dev \
  imagemagick

WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY . /myapp

CMD bundle exec puma