class VideosController < ApplicationController
  before_action :set_video, only: [:show]
  before_action :require_user
  # before_action :require_creator, only: [:edit, :update]

  def index
    # @videos = Video.all
    @categories = Category.all
  end

  def show
    @reviews = @video.reviews
  end

  def search
    @videos = Video.search_by_title(params[:term])
  end

  def advanced_search
    @videos = []
    options = {
      reviews: params[:reviews],
      rating_from: params[:rating_from],
      rating_to: params[:rating_to]
    }

    if params[:query]
      @videos = Video.search(params[:query], options).records.to_a
    end
  end

  private

  def set_video
    @video = VideoDecorator.decorate(Video.find(params[:id]))
  end
end