module StripeWrapper
  class Charge
    attr_reader :response, :error_message
    def initialize(options={})
      @response = options[:response]
      @error_message = options[:error_message]
    end

    def self.create(options={})
      begin
        res = Stripe::Charge.create({
          amount: options[:amount],
          currency: options[:currency],
          description: options[:description],
          source: options[:source]
        })
        new(response: res)
      rescue => e
        new(error_message: e.message)
      end
    end

    def successful?
      @response.present?
    end
  end

  class Customer
    attr_reader :response, :error_message
    def initialize(options={})
      @response = options[:response]
      @error_message = options[:error_message]
    end

    def self.create(options={})
      begin
        res = Stripe::Customer.create({
          email: options[:email],
          source: options[:source],
          description: "Customer for EcomRails"
        })
        new(response: res)
      rescue => e
        new(error_message: e.message)
      end
    end

    def self.delete(id)
      begin
        res = Stripe::Customer.delete(id)
        new(response: res)
      rescue => e
        new(error_message: e.message)
      end
    end

    def self.retrieve(user)
      begin
        Stripe::Customer.retrieve(user.customer_token)
      rescue => e
        nil
      end
    end

    def successful?
      @response.present?
    end

    def customer_token
      @response.id
    end
  end

  class Subscription
    attr_reader :response, :error_message
    def initialize(options={})
      @response = options[:response]
      @error_message = options[:error_message]
    end

    def self.create(options={})
      begin
        res = Stripe::Subscription.create({
          customer: options[:customer],
          items: [
            {
              plan: 'plan_F6D5e9ClW0TilM',
            },
          ],
        })
        new(response: res)
      rescue => e
        new(error_message: e.message)
      end
    end

    def self.retrieve(user)
      customer = StripeWrapper::Customer.retrieve(user)
      if customer
        begin
          Stripe::Subscription.retrieve(customer.subscriptions.data[0].id)
        rescue => e
          nil
        end
      end
    end

    def self.cancel_subscription(user)
      subscription = retrieve(user)
      if subscription
        begin
          res = Stripe::Subscription.delete(subscription.id)
          new(response: res)
        rescue => e
          new(error_message: e.message)
        end
      else
        new(error_message: "No subscription is found.")
      end
    end

    def successful?
      @response.present?
    end
  end

  class Invoice
    def self.list(user, limit=10)
      customer = StripeWrapper::Customer.retrieve(user)
      Stripe::Invoice.list(customer: customer, limit: limit).try(:data)
    end
  end
end