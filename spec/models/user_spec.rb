
require 'spec_helper'
require 'rails_helper'

describe User do
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email) }
  it { should validate_presence_of(:full_name) }
  it { should have_many(:queue_items) }
  it { should have_many(:reviews) }
  it { should have_many(:following_relationships) }

  it_behaves_like "tokenable" do
    let(:object) { Fabricate(:user) }
  end

  describe "#queued_video?" do
    it "returns true if the video is in the users queue" do
      user = Fabricate(:user)
      video = Fabricate(:video)
      Fabricate(:queue_item, user: user, video: video)
      expect(user.queued_video?(video)).to be_truthy
    end

    it "returns false if the video is not in the users queue" do
      user = Fabricate(:user)
      video = Fabricate(:video)
      video2 = Fabricate(:video)
      Fabricate(:queue_item, user: user, video: video2)
      expect(user.queued_video?(video)).to be_falsy
    end
  end

  describe "#follows?" do
    it "true if the current user already follows the user" do
      alice = Fabricate(:user)
      bob = Fabricate(:user)
      Fabricate(:relationship, leader: bob, follower: alice)
      expect(alice.follows?(bob)).to be_truthy
    end

    it "false if the current user does not follow the user" do
      alice = Fabricate(:user)
      bob = Fabricate(:user)
      expect(alice.follows?(bob)).to be_falsy
    end
  end

  describe "#follow" do
    it "follows an user" do
      alice = Fabricate(:user)
      bob = Fabricate(:user)
      alice.follow(bob)
      expect(alice.follows?(bob)).to be_truthy
    end

    it "does not follow self" do
      alice = Fabricate(:user)
      alice.follow(alice)
      expect(alice.follows?(alice)).to be_falsy
    end
  end

  describe "#deactivate!" do
    it "deactivates an active user" do
      alice = Fabricate(:user, active: true)
      alice.deactivate!
      expect(alice).not_to be_active
    end
  end
end