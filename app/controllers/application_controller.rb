class ApplicationController < ActionController::Base
  helper_method :current_user, :logged_in?

  # If there is already logged in (current user exists) return the user
  # otherwise get it from session
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end

  def require_user
    if !logged_in?
      flash[:danger] = "You need to login"
      redirect_to login_path
    end
  end

  def access_denied
    flash[:danger] = "You are not authorized to do that"
    redirect_to root_path
  end
end
