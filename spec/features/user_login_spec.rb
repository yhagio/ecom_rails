require 'spec_helper'
require 'rails_helper'

feature "user logs in" do
  scenario "with valid email and password" do
    alice = Fabricate(:user)
    user_login(alice)
    expect(page).to have_content alice.full_name
  end

  scenario "cannot login with deactivated user" do
    alice = Fabricate(:user, active: false)
    user_login(alice)
    expect(page).not_to have_content alice.full_name
    expect(page).to have_content("Your account has been deactivated. Please contact support.")
  end
end