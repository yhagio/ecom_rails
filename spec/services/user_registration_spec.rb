require 'spec_helper'
require 'rails_helper'

describe UserRegistration do
  describe "#register" do
    context "With invalid personal inputs" do
      before do
        customer = double(:customer)
        allow(StripeWrapper::Customer).to receive(:create).and_return(customer)
      end

      after do
        ActionMailer::Base.deliveries.clear
      end

      it "does not charge card" do
        UserRegistration.new(Fabricate.build(:user, email: nil)).register("token", nil)
        expect(StripeWrapper::Customer).not_to receive(:create)
      end

      it "does not send email" do
        UserRegistration.new(Fabricate.build(:user, email: nil)).register("token", nil)
        expect(ActionMailer::Base.deliveries).to be_empty
      end

      it "sets the error message" do
        result = UserRegistration.new(Fabricate.build(:user, email: nil)).register("token", nil)
        expect(result.error_message).to eq("Check your inputs, and try again")
      end
    end

    context "With valid personal inputs and declined credit card information" do
      before do
        customer = double(:customer, successful?: false, error_message: "Card is declined")
        allow(StripeWrapper::Customer).to receive(:create).and_return(customer)
      end

      after do
        ActionMailer::Base.deliveries.clear
      end

      it "does not creates user" do
        UserRegistration.new(Fabricate.build(:user)).register("token", nil)
        expect(User.count).to eq(0)
      end

      it "does not send email" do
        UserRegistration.new(Fabricate.build(:user)).register("token", nil)
        expect(ActionMailer::Base.deliveries).to be_empty
      end

      it "sets the error message" do
        result = UserRegistration.new(Fabricate.build(:user)).register("token", nil)
        expect(result.error_message).to eq("Card is declined")
      end
    end

    context "With valid personal inputs via invitation" do
      let(:customer) { double(:customer, successful?: true, customer_token: 'sdf_1234') }
      let(:charge) { double(:charge, successful?: true) }
      let(:alice) { Fabricate(:user) }
      let(:bob) { Fabricate.build(:user, email: "bob@cc.cc", full_name: "Bob Smith") }

      before do
        allow(StripeWrapper::Customer).to receive(:create).and_return(customer)
        allow(StripeWrapper::Subscription).to receive(:create).and_return(charge)
        invitation = Fabricate(:invitation, inviter: alice, recipient_email: "bob@cc.cc")
        UserRegistration.new(bob).register("token", invitation.token)
      end

      after do
        ActionMailer::Base.deliveries.clear
      end

      it "creates user" do
        expect(User.count).to eq(2)
      end

      it "stores customer token with the user" do
        expect(User.last.customer_token).to eq('sdf_1234')
      end

      it "sends out email to the user after user is saved" do
        expect(ActionMailer::Base.deliveries.last.to).to eq(['bob@cc.cc'])
      end

      it "sends out email containing full name after user is saved" do
        expect(ActionMailer::Base.deliveries.last.body).to include('Bob Smith')
      end

      it "make the user follow the inviter" do
        bob = User.find_by(email: "bob@cc.cc")
        expect(bob.follows?(alice)).to be_truthy
      end

      it "make the inviter follow the user" do
        bob = User.find_by(email: "bob@cc.cc")
        expect(alice.follows?(bob)).to be_truthy
      end

      it "expires the invitation after acceptance" do
        expect(Invitation.first.token).to be_nil
      end
    end
  end
end
