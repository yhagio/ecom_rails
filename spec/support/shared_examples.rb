RSpec.shared_examples "require log in" do
  it "redirects to login page" do
    session[:user_id] = nil
    action
    expect(response).to redirect_to login_path
  end
end

RSpec.shared_examples "tokenable" do
  it "generates random token when user is created" do
    expect(object.token).to be_present
  end
end

RSpec.shared_examples "requires admin" do
  it "redirects non-admin users to home" do
    session[:user_id] = Fabricate(:user).id
    action
    expect(response).to redirect_to root_path
  end
end
