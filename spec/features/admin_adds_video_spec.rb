require 'spec_helper'
require 'rails_helper'

feature "Admin adds video" do

  scenario "Admin successfully adds a new video" do
    admin_user = Fabricate(:admin)
    user_login(admin_user)
    upload_video

    user_logout
    user_login

    visit_uploaded_video
  end

  def upload_video
    category1 = Fabricate(:category)

    click_link "Add Video"
    fill_in 'Title', with: 'Monk'
    select category1.name, from: 'Category ids'
    fill_in 'Description', with: 'A cool movie'
    attach_file "Large cover", "spec/support/uploads/monk_large.jpg"
    attach_file "Small cover", "spec/support/uploads/monk.jpg"
    fill_in "Video URL", with: "https://www.youtube.com"
    click_button "Add Video"
  end

  def visit_uploaded_video
    visit video_path(Video.first)
    expect(page).to have_selector("img[src='/uploads/monk_large.jpg']")
    expect(page).to have_selector("a[href='https://www.youtube.com']")
  end
end