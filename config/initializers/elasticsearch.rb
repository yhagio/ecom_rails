Elasticsearch::Model.client =
  if Rails.env.staging? || Rails.env.production?
    Elasticsearch::Client.new url: ENV['SEARCHBOX_URL']
  elsif Rails.env.development?
    Elasticsearch::Client.new(log: true, host: ENV['ES_HOST'])
  else
    Elasticsearch::Client.new(host: ENV['ES_HOST'])
  end