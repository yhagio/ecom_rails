CREATE user dev_user with password '123test';
ALTER USER dev_user with superuser;

CREATE DATABASE ecom_rails_dev;
CREATE DATABASE ecom_rails_test;
GRANT all privileges ON DATABASE ecom_rails_dev TO dev_user;
GRANT all privileges ON DATABASE ecom_rails_test TO dev_user;
