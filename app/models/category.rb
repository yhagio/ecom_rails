class Category < ActiveRecord::Base
  default_scope { order('name ASC') }
  # has_many :videos

  has_many :video_categories
  has_many :videos, through: :video_categories

  validates :name, presence: true, uniqueness: true

  def recent_videos
    videos.order("created_at DESC").first(6)
  end
end